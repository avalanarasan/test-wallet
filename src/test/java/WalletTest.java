import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class WalletTest {
    @Test
    void shouldReturnTrueIfOneDollarIsEqualToItsInrEquivalent() {
        double dollar=1;
        double rupees=74.85;
        Wallet wallet = new Wallet(rupees,dollar);

        boolean expected=wallet.equivalent();

        assertTrue(expected);

    }

    @Test
    void shouldReturnWalletBalanceInRupeesAsPreferredCurrency() {
        String prefferedCurrency="Rs";
        double dollar=1;
        double rupees=50;
        double expected=124.85;


        Wallet wallet = new Wallet(rupees,dollar,prefferedCurrency);

        double actual=wallet.balanceInPrefferedCurrency();
        assertEquals(expected,actual);

    }

    @Test
    void shouldReturnWalletBalanceInDollarAsPreferredCurrency() {
        String prefferedCurrency="$";
        double dollar=1;
        double rupees1=74.85;
        double rupees2=149.7;
        double rupees=rupees1+rupees2;
        double expected=4;
        Wallet wallet = new Wallet(rupees,dollar,prefferedCurrency);

        double actual=wallet.balanceInPrefferedCurrency();

        assertEquals(expected,actual);
    }

    @Test
    void shouldCreditMoneyInWallet() {
        double dollar=1;
        double rupees=70;
        double expectedRupees=220;
        double expectedDollar=16;
        String creditCurrency1= "rs150";
        String creditCurrency2= "$15";
        Wallet wallet = new Wallet(rupees,dollar);

        wallet.creditCurrency(creditCurrency1);
        wallet.creditCurrency(creditCurrency2);
        assertAll(()->assertEquals(expectedRupees,wallet.getRupees()),
                ()-> assertEquals(expectedDollar,wallet.getDollar()));
    }


    @Test
    void shouldDebitMoneyFromWallet() throws InsufficientRupeesinWalletToDebitException {
        double dollar=5;
        double rupees=149.7;
        double pounds=4;
        String debitCurrency1="rs74.85";
        String debitCurrency2="$4";
        String debitCurrency3="&4";
        double expectedRupees=74.85;
        double expectedDollar=1;
        double expectedPound=0;
        Wallet wallet = new Wallet(rupees,dollar,pounds);

        wallet.debitCurrency(debitCurrency1);
        wallet.debitCurrency(debitCurrency2);
        wallet.debitCurrency(debitCurrency3);
        assertAll(()->assertEquals(expectedRupees,wallet.getRupees()),
                ()-> assertEquals(expectedDollar,wallet.getDollar()),
                ()-> assertEquals(expectedPound,wallet.getPounds()));

    }

    @Test
    void shouldThrowAmountInsuficientExceptionToWithdrawUnavailableRupees() {
        double dollar=5;
        double rupees=149.7;
        double debitRupees=224.5;
        Wallet wallet = new Wallet(rupees,dollar);

        assertThrows(InsufficientRupeesinWalletToDebitException.class,()->wallet.debitCurrency(debitRupees));

    }

    @Test
    void shouldThrowAmountInsuficientExceptionToWithdrawUnavailableDollar() throws InsufficientRupeesinWalletToDebitException {
        double dollar=5;
        double rupees=149.7;
        double debitDollar=24;
        Wallet wallet = new Wallet(rupees,dollar);

        wallet.debitCurrency(debitDollar);

    }

    //add pounds
    //any one currency credit or debit
    //add singapore dollar,euros
    //substring test case -credit and debit
    //solid principles -s
    //git pull rebase internal works
}
