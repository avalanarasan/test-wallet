public class Wallet {
    private double dollar;
    private double rupees;
    private double pounds;
    private static final double onedollarequivalentinrupees=74;
    private static final double onepoundsequivalentinrupees=102;
    private static final double onepoundsequivalentinrdollar=1.5;
    private String prefferedCurrency;

    public Wallet(double rupees, double dollar, String prefferedCurrency) {
        this.dollar = dollar;
        this.rupees = rupees;
        this.prefferedCurrency= prefferedCurrency;
    }
    public Wallet(double rupees, double dollar,double pounds) {
        this.dollar = dollar;
        this.rupees = rupees;
        this.pounds = pounds;
    }
    public Wallet(double rupees,double dollar){
        this.rupees = rupees;
        this.dollar = dollar;
    }

    public boolean equivalent() {
        return dollar==(rupees/onedollarequivalentinrupees);
    }
    public double convertDollarToRupees(){
        return dollar * onedollarequivalentinrupees;
    }
    public double convertPoundsToRupees(){return pounds/onepoundsequivalentinrupees;}
    public double convertRupeesToDollar(){
        return rupees/onedollarequivalentinrupees;
    }
    private double convertDollarToPound() {
        return  dollar*onepoundsequivalentinrdollar;
    }
    private double convertRupeesToPound() {
        return rupees/onepoundsequivalentinrupees;
    }
    private double convertPoundsToDollar() {
        return pounds*onepoundsequivalentinrdollar;
    }

    public double balanceInPrefferedCurrency() {
        if(prefferedCurrency.equals("Rs")){
                return rupees+convertDollarToRupees()+convertPoundsToRupees();
        }
        if(prefferedCurrency.equals("$")){
            double sum=dollar+convertRupeesToDollar()+convertPoundsToDollar();
            return (int) sum;
        }
        if(prefferedCurrency.equals("&")){
            return pounds+ convertDollarToPound()+convertRupeesToPound();
        }
        return 0;
    }
    public void creditCurrency(String currency) {
        if (currency.contains("rs")){
            String sbr =currency.substring(0, 2);
            if (sbr.equals("rs")){
            String acurrency=currency.substring(2);
            rupees+=Double.parseDouble(acurrency);
            }
        }
        if(currency.contains("$")){
            String sbr =currency.substring(0, 1);
            if(sbr.equals("$")) {
                String acurrency = currency.substring(1);
                dollar += Double.parseDouble(acurrency);
            }
            }
        if(currency.contains("&")){
            String sbr =currency.substring(0, 1);
            if(sbr.equals("&")) {
                String acurrency = currency.substring(1);
                pounds += Double.parseDouble(acurrency);
            }
        }

    }

    public double getRupees() {
        return rupees;
    }

    public int getDollar() {
        return (int) dollar;
    }

    public double getPounds(){return pounds;}

    public void debitCurrency(String currency) {
        if (currency.contains("rs")){
            String acurrency=currency.substring(2);
            rupees-=Double.parseDouble(acurrency);
        }
        if(currency.contains("$")){
            String acurrency=currency.substring(1);
            dollar-=Double.parseDouble(acurrency);
        }
        if(currency.contains("&")){
            String acurrency=currency.substring(1);
            pounds-=Double.parseDouble(acurrency);
        }

    }

    public void debitCurrency(double debitRupees) throws InsufficientRupeesinWalletToDebitException {
        if(rupees==0 || rupees < debitRupees) {
            throw new InsufficientRupeesinWalletToDebitException();
        }
        rupees-=debitRupees;
    }
    //preferred,debit,credit
    //actual - method
    //rebase when conflict
}
